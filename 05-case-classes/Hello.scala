// * case class ❤️

object Hello extends App {

  // TODO: define case class Toon with `avatar` property
  case class Toon(val avatar: String) {}


  // TODO: babs is a rabbit, buster is a rabbit, pandy is a panda
  val babs = Toon("🐰")
  val buster = Toon("🐰")
  val pandy = Toon("🐼")

  println(babs == buster) // compare by value true
  println(babs == pandy) // false

}

Hello.main(null)
