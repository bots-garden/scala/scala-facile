# Solutions

```scala
object Hello extends App {

  val aliments = List("🍞", "🍃", "🍅", "🍖", "🍟", "😭")

  // === action recette ===
  val decouper = (aliment: String) => s"morceaux de $aliment"

  // === choix aliments ===
  val sansFrite = (aliment: String) => aliment != "🍟"
  val sansOignon = (aliment: String) => aliment != "😭"

  // === filtrer, puis découper ===
  val kebab = aliments
                .filter(sansFrite).filter(sansOignon)
                .map(decouper)
                .fold("my 🥙 :\n") { (tmp, ingredient) => tmp + ingredient + "\n" }

  println(kebab) 

}

Hello.main(null)
```