# Solutions

```scala
object Hello extends App {
  
  case class Toon(name: String, avatar: String)
    
  val getToon = (name: String) => List(
      Toon("po", "🐼"),
      Toon("buster", "🐰"),
      Toon("simba", "🦁")        
    ).find(toon => toon.name == name) // find always returns an Option

  getToon("po") match {
    case None => println("😡")
    case Some(toon) => println(toon.avatar)
  }

  println(
    getToon("po").map(toon => toon.avatar).getOrElse("😡")
  )

}

Hello.main(null)

```