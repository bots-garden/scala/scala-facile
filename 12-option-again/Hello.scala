object Hello extends App {
  
  case class Toon(name: String, avatar: String)
    
  val getToon = (name: String) => List(
      Toon("po", "🐼"),
      Toon("buster", "🐰"),
      Toon("simba", "🦁")        
    ).find(toon => toon.name == name) // find always returns an Option

  // TODO: match
  getToon("po") match {
    case None => println("nope")
    case Some(value) => println(value.avatar)
  }

  // TODO: map 
  println(
    getToon("buster").map(toon => toon.avatar).getOrElse("😡")
  )

}

Hello.main(null)
