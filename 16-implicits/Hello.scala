// * --- 🔮 Implicits - Black Magic 😈 ---
object animals {
	class Animal(val name: String) {
	  def hello() = { println(s"Hello I'm $name") }
	}
	
	class Dog(name: String) extends Animal(name) {
	  def wouaf() = { println("wouaf I'm a 🐶") }
	}
}

object magic {
	implicit class AnimalImprovement(val beast: animals.Animal) {
	  def hey() = { println(s"Hey 👋 I'm ${beast.name} 😉") }
	}
}

import animals._
import magic._

object Hello extends App {

	val wolf = new Dog("wolf")
	wolf.wouaf

	wolf.hey

}

Hello.main(null)
