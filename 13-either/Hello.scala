// * --- Either ---

import scala.util.{Either, Left, Right}

object Hello extends App {

  def divide(a: Int, b:Int): Either[String, Int] = {
    try {
      Right(a/b)
    } catch {
      case e: Exception => Left(s"😡 ${e.toString}")
    }
  }

  divide(3, 0) match {
    case Left(message) => println(message)
    case Right(value) => println(value)
  }

}

Hello.main(null)