# [fit] Mais 😂, 
# [fit] pourquoi avoir inventé Kotlin,
# [fit] Scala 
# [fit] est déjà un Java Simplifié

# [fit] `Voxxed Days Luxembourg 2018`

---
# [fit]Merci 🙇🏻‍♂️ à vous 
# [fit]& @VoxxedDayLuxembourg
**Philippe Charrière**
CSO @ Clever Cloud 💡☁️
TAM @ GitLab 🦊
"CEO" 🤣 @ Bots.Garden 🤖🌼

### 🐦 @k33g_org

---

## Préjugés sur Scala

# [fit]🧙‍♂️

## bon en math

## illisible

## ...

---

# [fit] Bullshit 🐮 💩 ❗️

---
# [fit] Ok, ça fait 🌡 les 💻

---

# [fit] mais Scala
# [fit] c'est aussi simple que du
# [fit] JavaScript 🙀
# [fit] *avec un peu de typage...*

---
## [fit] Pression 😬

![right,fit](pression.png)

---

## [fit] 😜 Même pas mal ‼️

---
# [fit] 👩‍🎓 Un tuto avec
# [fit] 17 exemples pour
# [fit] apprendre Scala
# [fit] et trouver ça simple 

---
# Ressources

![fit](grokking.png)
![fit](programming.png)
![fit](functional.png)

---

![fit](grokking.png)
![fit](programming.png)
![fit](functional.png)

---

https://scastie.scala-lang.org/
http://ammonite.io/
