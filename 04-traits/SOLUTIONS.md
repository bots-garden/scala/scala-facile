# Solutions

```scala
//package demo

object people {

  // * empty class
  class SuperHero(val name: String) { /* 👻 */ }

  object powers {

    trait RunFast {
      def name: String
      def run() = { println(s"$name runs fast") }
    }

  }

  import people.powers._

  class Speedster(name: String) 
    extends SuperHero(name) 
    with RunFast { }

}

import people._
import people.powers._

object Hello extends App {

  // TODO: flash is a Speedster and he runs
  val flash = new Speedster("Flash")
  flash.run

  // TODO: nora is a SuperHero and she runs
  val nora = new SuperHero("Nora") with RunFast
  nora.run


}

Hello.main(null)

```

