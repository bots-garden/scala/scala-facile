object Hello extends App {

  val multiplication = (a: Int, b: Int) => a * b

  multiplication(21, 2) match {
    case 42 => println("😀")
    case 21 => println("😡")
    // ! with a guard
    case res if res > 21 => println("🎃")
    case _ => println("🤔")
  }

}

Hello.main(null)