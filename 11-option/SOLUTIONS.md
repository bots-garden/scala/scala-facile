# Solutions

```scala
object Hello extends App {
  
  val hello = Option("Hello")
  println(hello)
  println(hello.getOrElse("Hi"))

  val result = hello match {
    case None => "Rien du tout"
    case Some(x) => x
  }

  println(result)

}

Hello.main(null)
```