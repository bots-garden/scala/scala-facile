object Hello extends App {
  
  val hello = Option(null)
  println(hello)
  println(hello.getOrElse("Hi"))

  /*
  val result = hello match {...}

  println(result)
  */

}

Hello.main(null)
