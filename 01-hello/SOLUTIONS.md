# Solutions

```scala
//package demo

object Hello extends App {

  println("👋 🌍")
  println("I ❤️ Scala")
  println("Hello Voxxed 😜👋")

  // * --- variables --- 
  /*
  var firstName = "Bob"
  println(firstName)
  firstName = "Bobby"
  println(firstName)
  */

  // * --- values --- 
  // ! immutability
  /*
  val lastName = "Morane"
  println(lastName)
  lastName = "Ballantine"
  println(lastName)
  */

  // * --- String Interpolation ---
  //println(s"Bonjour ${firstName} ${lastName}")

  // * --- Control Structures ---
  /*
  if (firstName == "Bob") {
    println("Hello Bob")
  } else {
    println("You are not Bob")
  }

  if (firstName == "Bob" && lastName == "Morane") {
    println("Hello Bob Morane")
  } else {
    println("You are not Bob")
  }
  */

  // * --- Loops ---
  /*
  var a = 0
  while (a < 5) {
    println(a)
    a+=1
  }
  println("---------------")
  do {
    println(a)
    a+=1
  } 
  while (a < 10)
  println("---------------")
  for (i <- 1 to 3) println(i)
  println("---------------")
  // with guards

  for (i <- 1 to 10 if i < 6) println(i)

  */

}

// * more -> https://alvinalexander.com/scala/scala-for-loop-examples-syntax-yield-foreach

Hello.main(null)

```