import scala.util.{Try, Failure, Success}

object Hello extends App {

  def divide(a: Int, b:Int): Try[Int] = Try(a/b)

  divide(3, 0) match {
    case Failure(err) => println(s"😡 ${err.getMessage}")
    case Success(value) => println(value)
  }

  divide(4, 2) match {
    case Failure(err) => println(s"😡 ${err.getMessage}")
    case Success(value) => println(value)
  }  

}

Hello.main(null)
