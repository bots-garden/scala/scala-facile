object Hello extends App {

  def addition(a: Int, b:Int): Int = { a + b }
  def division(a: Int, b:Int): Int =  a / b

  println(addition(35,7))
  println(division(84,2))

  // * Lambdas
  val multiplication = (a: Int,b: Int) => a * b
  
  println(multiplication(21,2))

  val multiply = (a: Int) => {
    (b:Int) => a * b
  }
  
  println(
    multiply(5)(6) + multiply(6)(2)
  )

  val multiplyBy5 = multiply(5)
  val multiplyBy2 = multiply(2)

  println(
    multiplyBy5(6) + multiplyBy2(6)
  )

}

Hello.main(null)
